<?php
/**
 * @file
 * Contains admin logic for the Apache Solr GetSatisfaction integration.
 */
use GuzzleHttp\Client;

/**
 * Creates an admin form for the module.
 */
function apachesolr_getsat_admin() {
  $form = array();
  $environments = apachesolr_load_all_environments();
  $environments_options = array();

  foreach ($environments as $env_id => $environment) {
    $environments_options[$env_id] = $environment['name'];
  }

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general']['apachesolr_getsat_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Integration'),
    '#default_value' => variable_get('apachesolr_getsat_enabled', false),
  );

  $form['general']['apachesolr_getsat_company'] = array(
    '#type' => 'textfield',
    '#title' => t('Company code on GetSatisfaction'),
    '#default_value' => variable_get('apachesolr_getsat_company', ''),
    '#size' => 20,
    '#maxlength' => 60,
    '#description' => t('This is the company code that can be found in the URL to the community page.'),
    '#required' => TRUE,
  );

  $form['general']['apachesolr_getsat_queue_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount to process per cron'),
    '#default_value' => variable_get('apachesolr_getsat_queue_limit', APACHESOLR_GETSAT_QUEUE_LIMIT_DEFAULT),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('This is how much of the queue to process each cron run.'),
    '#required' => TRUE,
  );

  $form['general']['apachesolr_getsat_api_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Topics per API request'),
    '#default_value' => variable_get('apachesolr_getsat_api_limit', APACHESOLR_GETSAT_TOPIC_PER_API_REQUEST),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('This is how many topics will be added to the queue per API request.'),
    '#required' => TRUE,
  );

  $form['general']['apachesolr_getsat_solr_index'] = array(
    '#type' => 'select',
    '#title' => t('Target Index'),
    '#default_value' => variable_get('apachesolr_getsat_solr_index', NULL),
    '#options' => $environments_options,
    '#required' => TRUE,
    '#description' => t('This will be the Solr index your GetSatisfaction results will be added to.'),
  );

  $form['query'] = array(
    '#type' => 'fieldset',
    '#title' => t('Topic Filtering Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['query']['apachesolr_getsat_query_styles'] = array(
    '#type' => 'select',
    '#title' => t('Filter by topic styles'),
    '#default_value' => variable_get('apachesolr_getsat_query_styles', NULL),
    '#options' => array(
      'question' => t('Question'),
      'problem' => t('Problem'),
      'praise' => t('Praise'),
      'idea' => t('Idea'),
      'update' => t('Update'),
    ),
    '#multiple' => TRUE,
    '#description' => t('Only selected Topic styles will be added to your index. If none are selected, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_status'] = array(
    '#type' => 'select',
    '#title' => t('Filter by topic status'),
    '#default_value' => variable_get('apachesolr_getsat_query_status', NULL),
    '#options' => array(
      'none' => t('None'),
      'pending' => t('Pending'),
      'active' => t('Active'),
      'complete' => t('Complete'),
      'rejected' => t('Rejected'),
    ),
    '#multiple' => TRUE,
    '#description' => t('Only selected Topic statuses will be added to your index. If none are selected, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter by general search'),
    '#default_value' => variable_get('apachesolr_getsat_query_term', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Only Topics found by this search term will be added to your index. If no term is given, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_product'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter by product'),
    '#default_value' => variable_get('apachesolr_getsat_query_product', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Only Topics on this product will be added to your index. Multiple products can be added by separating with commas. If no product(s) is given, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter by tag'),
    '#default_value' => variable_get('apachesolr_getsat_query_tag', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Only Topics with this tag will be added to your index. If no tag is given, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_private_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter by private tags'),
    '#default_value' => variable_get('apachesolr_getsat_query_private_tag', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Only Topics with this private tag will be added to your index. Multiple private tags can be added by separating with commas. If no product(s) is given, filter will be ignored.'),
  );

  $form['query']['apachesolr_getsat_query_archived'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Archived'),
    '#default_value' => variable_get('apachesolr_getsat_query_archived', false),
    '#description' => t('If this box is checked, archived topics will be added to your index.'),
  );

  $form['query']['apachesolr_getsat_query_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Age Limit'),
    '#date_format' => APACHESOLR_GETSAT_DATE_FORMAT,
    '#date_label_position' => 'within',
    '#date_timezone' => 'America/Chicago',
    '#date_increment' => 15,
    '#date_year_range' => '-10:0',
    '#default_value' => variable_get('apachesolr_getsat_query_date', '2016-01-01 00:00'),
    '#description' => t('Only topics modified after this time will be added to the indexing queue. This value will be updated by cron and normally should not be altered.'),
  );

  return system_settings_form($form);
}

/**
 * Validates User input in the administrator form by pinging the API and
 * ensuring results are present.
 *
 * @param $form
 * @param $form_state
 */
function apachesolr_getsat_admin_validate($form, &$form_state) {
  $getsat_company = $form_state['values']['apachesolr_getsat_company'];

  if ((int) ($form_state['values']['apachesolr_getsat_query_date']) > APACHESOLR_GETSAT_MAX_PAGE) {
    form_set_error('form', t('GetSatisfaction API only supports @max results per page.', array('@max' => APACHESOLR_GETSAT_MAX_PAGE)));
  }

  $client = new Client([
    'base_uri' => APACHESOLR_GETSAT_URL,
  ]);

  $query = apachesolr_getsat_api_params(array(
    'query' => $form_state['values']['apachesolr_getsat_query_term'],
    'style' => $form_state['values']['apachesolr_getsat_query_styles'],
    'product' => $form_state['values']['apachesolr_getsat_query_product'],
    'tag' => $form_state['values']['apachesolr_getsat_query_tag'],
    'user_defined_code' => $form_state['values']['apachesolr_getsat_query_private_tag'],
    'status' => $form_state['values']['apachesolr_getsat_query_status'],
    'include_archived' => $form_state['values']['apachesolr_getsat_query_archived'],
    'active_since' => $form_state['values']['apachesolr_getsat_query_date'],
  ));

  $response = NULL;

  try {
    $response = $client->get(
      "companies/$getsat_company/topics.json",
      array(
        'query' => $query,
      )
    );

    $response = json_decode($response->getBody());
  } catch (Exception $e) {
    form_set_error('form', t('GetSatisfaction API returned a error.'));
  }

  if (empty($response)) {
    form_set_error('form', t('GetSatisfaction API returned no response.'));
  }

  if (!isset($response->total) || $response->total == 0) {
    form_set_error('form', t('GetSatisfaction API returned no results, check your filters and try again.'));
  }
  else {
    drupal_set_message(
      t(
        'GetSatisfaction API returned @result_count with the current parameters.',
        array('@result_count' => $response->total)
      )
    );
  }
}